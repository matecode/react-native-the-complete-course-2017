import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

class App extends Component {
  componentWillMount() {
    const firebaseConfig = {
      apiKey: 'AIzaSyC4svYkHBYVKk7uC-AJW7RvCvkx_Zly43I',
      authDomain: 'react-native-course-mana-c8988.firebaseapp.com',
      databaseURL: 'https://react-native-course-mana-c8988.firebaseio.com',
      storageBucket: 'react-native-course-mana-c8988.appspot.com',
      messagingSenderId: '858520982814'
    };
    firebase.initializeApp(firebaseConfig);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
        <Router />
        
      </Provider>
    );
  }
}

export default App;
